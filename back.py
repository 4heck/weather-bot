import json

import requests
from flask import Flask, request, make_response, jsonify
from geopy.geocoders import Nominatim

app = Flask(__name__)

API_KEY = '7bbd501bec197a0d584ae69948ac908d'


@app.route('/')
def index():
    return 'Привет!'


def results():
    print(request)
    req = request.get_json(force=True)

    _action = req.get('queryResult').get('action')

    result = req.get("queryResult")
    parameters = result.get("parameters")

    if parameters.get('location').get('city'):
        geolocator = Nominatim(user_agent='weather-bot')
        city = parameters.get('location').get('city')
        location = geolocator.geocode(city)
        lat = location.latitude
        long = location.longitude
        weather_req = requests.get(
            'https://api.openweathermap.org/data/2.5/onecall?lat={}&lon={}&appid={}'.format(lat, long, API_KEY))
        current_weather = json.loads(weather_req.text)['current']
        temp = round(current_weather['temp'] - 273.15)
        feels_like = round(current_weather['feels_like'] - 273.15)
        clouds = current_weather['clouds']
        wind_speed = current_weather['wind_speed']

        weather_text = f'Погода в городе {str(city)}\n\nТемпература воздуха - {str(temp)} градусов,' \
                       f' ощущается как {str(feels_like)} ' \
                       f'градусов, облачность - {str(clouds)}%, скорость ветра - {str(wind_speed)}м/с' \
                       f' \n\nP.S. Если что-то работает криво - заскриншоть и напиши @for_check :)'

    return {
        'fulfillmentText': weather_text
    }


@app.route('/webhook', methods=['GET', 'POST'])
def webhook():
    response = make_response(jsonify(results()))
    return response


if __name__ == '__main__':
    app.run(debug=True)
